port module Ports exposing (askForCamera, takePhoto, cameraUsageGranted, textProcessingFinished, textProcessingUpdate, reactivateCamera, TesseractProgress, TesseractResult)

type alias TesseractProgress = 
    { progress: Float
    , status: String
    }

type alias TesseractResult = 
    { text: String
    , confidence: Float
    }

port askForCamera : () -> Cmd a

port takePhoto : () -> Cmd a

port cameraUsageGranted : (Bool -> msg) -> Sub msg

-- TODO: Add type to follow progress
port textProcessingUpdate : (TesseractProgress -> msg) -> Sub msg
port textProcessingFinished : (TesseractResult -> msg) -> Sub msg
port reactivateCamera : () -> Cmd a
