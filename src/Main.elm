module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Html exposing (Html, button, div, h2, h3, img, text, p)
import Html.Attributes exposing (src, style, class)
import Html.Events exposing (onClick)
import Ports exposing (askForCamera, takePhoto, cameraUsageGranted, textProcessingFinished, textProcessingUpdate, reactivateCamera, TesseractProgress, TesseractResult)

---- MODEL ----

type Model
    = WaitingForCamera
    | CameraActivated
    | PhotoTaken TesseractProgress
    | PhotoProcessed TesseractResult

init : ( Model, Cmd Msg )
init =
    ( WaitingForCamera
    , Cmd.none
    )

---- UPDATE ----

type Msg
    = AskForCamera
    | TakePhoto
    | CameraUsageGranted Bool
    | ProgressUpdate TesseractProgress
    | ResultProcessed TesseractResult
    | ReactivateCamera

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AskForCamera ->
            ( WaitingForCamera, askForCamera () )

        TakePhoto ->
            ( PhotoTaken (TesseractProgress 0 "Initializing"), takePhoto () )

        CameraUsageGranted bool ->
            ( CameraActivated, Cmd.none )

        ProgressUpdate progress ->
            ( PhotoTaken progress, Cmd.none )

        ResultProcessed res ->
            ( PhotoProcessed res, Cmd.none)

        ReactivateCamera ->
            ( CameraActivated, reactivateCamera () )

---- SUBSCRIPTION ----

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch 
        [ cameraUsageGranted (\granted -> CameraUsageGranted granted)
        , textProcessingFinished (\result -> ResultProcessed result)
        , textProcessingUpdate (\progress -> ProgressUpdate progress)
        ]

---- VIEW ----

view : Model -> Html Msg
view model =
    case model of 
        WaitingForCamera -> activateCameraView
        CameraActivated -> button [ onClick TakePhoto ] [ text "Process image" ]
        PhotoTaken progress -> div [ ] 
            [ progressSpinner
            , p [ class "result" ] [ text (progress.status ++ String.fromFloat progress.progress) ]
            ]
        PhotoProcessed result -> 
            div [ ] 
            [ button [ onClick ReactivateCamera ] [ text "Take new photo" ] 
            , if String.isEmpty result.text then 
                p [ class "result" ] [ text "Aucun résultat" ]
              else
                div [] 
                    [ h2 [ ] [ text "Result" ]
                    , h3 [ ] [ text ("Confidence : " ++ String.fromFloat result.confidence ++ "/100") ]
                    , p [ class "result" ] [ text result.text]
                    ]
            ]

activateCameraView : Html Msg
activateCameraView =
    div [ class "camera-activation-div" ] 
        [ img [ src "./camera.svg"
            , style "color" "grey" 
            ] [ ]
        , button [ onClick AskForCamera ] [ text "Activate camera" ]
        ]

progressSpinner : Html Msg
progressSpinner = 
    div [ class "lds-ellipsis"] 
        [ div [] []
        , div [] []
        , div [] []
        , div [] []
        ]

---- PROGRAM ----

main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = subscriptions
        }
