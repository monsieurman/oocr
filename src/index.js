import './main.css';
import {
  Elm
} from './Main.elm';
import registerServiceWorker from './registerServiceWorker';
import Tesseract from 'tesseract.js';
const {
  TesseractWorker
} = Tesseract;
const worker = new TesseractWorker();

const app = Elm.Main.init({
  node: document.getElementById('root')
});

registerServiceWorker();

const constraints = {
  video: true,
  audio: false
};

const video = document.querySelector('video');

app.ports.askForCamera.subscribe(() => {
  navigator.mediaDevices.getUserMedia(constraints)
    .then((stream) => {
      video.srcObject = stream;
      app.ports.cameraUsageGranted.send(true);
    })
    .catch((reason) => {
      app.ports.cameraUsageGranted.send(false);
    })
    .finally(() => {
      console.log('Closed ?');
    });
});

app.ports.takePhoto.subscribe(() => {
  video.pause();
  const canvas = document.createElement('canvas');
  canvas.getContext('2d')
    .drawImage(video, 0, 0, canvas.width, canvas.height);
  const img = new Image();
  img.src = canvas.toDataURL();

  worker.recognize(img)
    .progress((pro) => {
      console.log(pro)
      app.ports.textProcessingUpdate.send({
        status: pro.status,
        progress: pro.progress
      });
    })
    .then((res) => {
      // TODO: see if we have other insight from the result we should send to user.
      console.log(res);
      app.ports.textProcessingFinished.send({
        text: res.text,
        confidence: res.confidence
      });
    })
    .catch((error) => {
      alert(`you did not catch this error ${error}`);
    })
});

app.ports.reactivateCamera.subscribe(() => {
  video.play();
});
